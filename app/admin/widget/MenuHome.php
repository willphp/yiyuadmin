<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 根据菜单ID获取菜单列表
class MenuHome extends Widget
{
    protected string $tag = 'menu';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        return db('menu')->field('id,title,icon,href')->where('id', 'in', $id)->order('sort ASC,id ASC')->select();
    }
}