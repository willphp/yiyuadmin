<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2025 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\admin\controller;
use aphp\core\Jump;
use aphp\core\Tool;

// 主题管理
class Tpl
{
    use Jump;
    protected string $middleware = 'rbac';
    protected string $apiUrl = 'https://api.aphpcms.com'; // 远程api地址
    protected string $tplPath; // 前台模板目录
    protected string $resPath; // 主题资源目录

    public function __construct()
    {
        $this->tplPath = ROOT_PATH . '/template';
        $this->resPath = ROOT_PATH . '/public/static/themes';
    }

    public function index()
    {
        if ($this->isAjax()) {
            $list = array_values($this->get_local()); // 获取本地主题
            $this->_json(200, '', $list, ['count' => count($list)]);
        }
        return view();
    }

    // 远程主题
    public function online()
    {
        if ($this->isAjax()) {
            $list = array_values($this->api_get_online());
            $this->_json(200, '', $list, ['count' => count($list)]);
        }
        view_with('site', ['name' => 'aphp']);
        $notice = cache_make('theme_notice',fn()=>$this->api_get_notice(), 600);
        return view()->with('notice', $notice);
    }

    // 设置(使用)前台主题
    public function use(string $name)
    {
        $r = $this->set_theme($name);
        $this->_jump(['主题设置成功', '请先安装此主题'], $r);
    }

    // 导出主题
    public function export(string $name)
    {
        $zip_file = $this->tplPath . '/aphp.' . $name . '.zip';
        if (file_exists($zip_file)) {
            unlink($zip_file);
        }
        Tool::dir_copy($this->resPath . '/' . $name, $this->tplPath . '/' . $name . '/_assets');
        $phar = new \PharData($zip_file);
        $phar->buildFromDirectory($this->tplPath . '/' . $name);
        Tool::dir_delete($this->tplPath . '/' . $name . '/_assets', true);
        $this->success('主题导出成功！文件：template/aphp.' . $name . '.zip');
    }

    // 卸载主题
    public function uninstall(string $name)
    {
        if ($name == 'default') {
            $this->error('默认主题不允许卸载');
        }
        if (!is_dir($this->tplPath . '/' . $name)) {
            $this->error('此主题未安装');
        }
        Tool::dir_delete($this->resPath . '/' . $name, true);
        Tool::dir_delete($this->tplPath . '/' . $name, true);
        $this->success('主题卸载成功');
    }

    // 删除主题
    public function delete(string $name)
    {
        if ($name == 'default') {
            $this->error('默认主题不允许删除');
        }
        $zip_file = $this->tplPath . '/aphp.' . $name . '.zip';
        !file_exists($zip_file) || unlink($zip_file);
        $this->success('删除成功');
    }

    // 更新主题
    public function update(string $name)
    {
        Tool::dir_delete($this->resPath . '/' . $name, true);
        Tool::dir_delete($this->tplPath . '/' . $name, true);
        $zip_file = $this->tplPath . '/aphp.' . $name . '.zip';
        !file_exists($zip_file) || unlink($zip_file);
        $this->install($name);
    }

    // 安装主题
    public function install(string $name)
    {
        $zip_file = $this->tplPath . '/aphp.' . $name . '.zip';
        if (!file_exists($zip_file)) {
            // 远程下载文件
            $down_url = $this->api_get_down_url($name);
            if (!empty($down_url)) {
                $code = $this->get_down_code($down_url, $zip_file);
                if ($code !== 200) {
                    $code_info = $this->api_get_code_info();
                    $msg = $code_info[$code] ?? '请稍后再试';
                    $this->error('下载安装包失败，'.$msg.'！');
                }
            } else {
                $this->error('主题安装包文件不存在');
            }
        }
        if (is_dir($this->tplPath . '/' . $name . '/')) {
            $this->error('主题已存在，请卸载后再安装！');
        }
        $zip = new \ZipArchive();
        if ($zip->open($zip_file) === TRUE) {
            $zip->extractTo($this->tplPath . '/' . $name . '/');
            $zip->close();
            Tool::dir_move($this->tplPath . '/' . $name . '/_assets/', $this->resPath . '/' . $name . '/');
            // 更新主题ini配置
            $online = $this->api_get_online();
            if (isset($online[$name])) {
                $online[$name]['image'] = '/static/themes/' . $name . '/screenshot.jpg';
                unset($online[$name]['show']);
                file_put_contents($this->tplPath . '/' . $name . '/ini.php', "<?php\nreturn " . var_export($online[$name], true) . ';');
            }
            $this->success('主题安装成功', 'index');
        }
        $this->error('主题安装失败');
    }

    // 获取主题下载地址
    protected function api_get_down_url(string $name): string
    {
        $ret = get_curl($this->apiUrl . '/theme/zip?name=' . $name);
        if (!empty($ret) && json_validate($ret)) {
            $ret = json_decode($ret, true);
            return $ret['data']['file'] ?? '';
        }
        return '';
    }

    // 获取下载返回状态码信息
    protected function api_get_code_info(): array
    {
        $ret = get_curl($this->apiUrl . '/theme/code');
        if (!empty($ret) && json_validate($ret)) {
            $ret = json_decode($ret, true);
            return $ret['data'];
        }
        return [];
    }

    // 下载安装包 200 下载成功 400 curl失败 404 文件不存在 405 非法获取 406 文件写入失败
    protected function get_down_code(string $url, string $save_file): int
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, $this->apiUrl);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50); // 设置超时时间
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 不验证ssl证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // 不验证主机名
        $file_content = curl_exec($ch);
        curl_close($ch);
        if ($file_content === false) {
            return 400; // curl失败
        }
        if (is_numeric($file_content)) {
            return (int) $file_content;
        }
        return file_put_contents($save_file, $file_content) === false ? 406 : 200;
    }

    // 添加自定义主题
    public function add(array $req)
    {
        if ($this->isPost()) {
            $sign = $req['sign'];
            $theme_path = Tool::dir_init($this->tplPath . '/' . $sign);
            if (file_exists($theme_path . '/ini.php')) {
                $this->error('该主题已存在！');
            }
            $req['id'] = 0;
            $req['image'] = '/static/themes/' . $sign . '/screenshot.jpg';
            $req['price'] = $req['price'] > 0 ? '￥' . $req['price'] : '免费';
            $req['sort'] = 9999;
            $req['time'] = date('Y-m-d');
            file_put_contents($theme_path . '/ini.php', "<?php\nreturn " . var_export($req, true) . ';');
            $res_path = Tool::dir_init($this->resPath . '/' . $sign);
            Tool::file_copy(ROOT_PATH . '/public/static/image/screenshot.jpg', $res_path);
            $this->success('主题添加成功！', 'index');
        }
        return view();
    }

    // 获取远程主题信息
    protected function api_get_online(): array
    {
        $ret = get_curl($this->apiUrl . '/theme/index');
        if (!empty($ret) && json_validate($ret)) {
            $ret = json_decode($ret, true);
            if ($ret['status'] == 1) {
                $installed = array_filter(scandir($this->tplPath), fn(string $file): bool => is_dir($this->tplPath . '/' . $file) && $file != '.' && $file != '..');
                foreach ($ret['data'] as &$vo) {
                    $vo['show'] = in_array($vo['sign'], $installed) ? 4 : 5;
                }
                return $ret['data'];
            }
        }
        return [];
    }

    // 获取远程主题公告
    protected function api_get_notice(): string
    {
        $ret = get_curl($this->apiUrl . '/theme/notice');
        if (!empty($ret) && json_validate($ret)) {
            $ret = json_decode($ret, true);
            return $ret['data']['notice'] ?? '';
        }
        return '';
    }

    // 获取本地主题
    protected function get_local(): array
    {
        $local = [];
        $current_name = $this->get_current_name(); // 当前主题名
        $online = $this->api_get_online(); // 远程主题列表
        $dir = $this->tplPath; // 模板路径
        $list = array_diff(scandir($dir), ['.', '..']); // 本地主题名列表
        $local_zip = []; // 本地安装包列表
        foreach ($list as $file) {
            if (is_dir("$dir/$file") && file_exists("$dir/$file/ini.php")) {
                // 读取本地信息
                $local[$file] = include "$dir/$file/ini.php";
                if ($current_name == $file) {
                    $local[$file]['sort'] = 0;
                    $local[$file]['show'] = 1;
                } else {
                    $local[$file]['show'] = 2;
                }
                // 比效版本
                if (isset($online[$file]['version']) && isset($local[$file]['version'])) {
                    if (version_compare($local[$file]['version'], $online[$file]['version'], '<')) {
                        // 更新标识
                        $local[$file]['new'] = 1;
                    }
                }
            } elseif (preg_match('/aphp.(.*).zip/', $file, $match)) {
                $local_zip[] = $match[1]; // 本地安装包
            }
        }
        foreach ($local_zip as $sign) {
            if (isset($local[$sign])) {
                // 本地主题(已安装，有安装包)
                $local[$sign]['show'] = ($current_name == $sign) ? 1 : 2;
            } elseif (isset($online[$sign])) {
                // 本地主题安装包(未安装)，在线主题列表中
                $local[$sign] = $online[$sign];
                $local[$sign]['show'] = 3;
            } else {
                // 本地主题安装包(未安装)，不在线主题列表中
                $local[$sign] = [
                    'id' => 0,
                    'sign' => $sign,
                    'title' => $sign . '主题',
                    'image' => '/static/image/screenshot.jpg',
                    'intro' => '此模板为自定主题！',
                    'version' => '1.0',
                    'price' => '免费',
                    'author' => '未知(待安装)',
                    'link' => 'https://www.aphpcms.com',
                    'sort' => 9999,
                    'time' => date('Y-m-d'),
                    'show' => 3
                ];
            }
        }
        uasort($local, function ($a, $b) {
            return $a['sort'] <=> $b['sort'];
        });
        return $local;
    }

    // 获取前台当前主题名称
    protected function get_current_name(): string
    {
        $theme = [];
        $theme_file = ROOT_PATH . '/app/index/config/theme.php';
        if (file_exists($theme_file)) {
            $theme = require $theme_file;
        }
        return $theme['name'] ?? 'default';
    }

    // 设置前台使用主题
    protected function set_theme(string $name): bool
    {
        if (!is_dir($this->tplPath . '/' . $name)) {
            return false;
        }
        $theme_file = ROOT_PATH . '/app/index/config/theme.php';
        Tool::dir_init(dirname($theme_file));
        $theme = ['name' => $name];
        return (bool)file_put_contents($theme_file, "<?php\nreturn " . var_export($theme, true) . ';');
    }

}