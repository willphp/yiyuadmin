<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
class Config extends Model
{
    protected string $table = 'config';
    protected string $pk = 'id';
    protected array $validate = [
        ['group_id', '/^[1-9]\d*$/', '请选择分组', IF_MUST, AC_BOTH],
        ['field_title', 'required', '标题必须', IF_MUST, AC_BOTH],
        ['field_name', '/^\w+$/|unique', '键名必须是英文|键名已存在', IF_MUST, AC_BOTH],
        ['sort', 'number', '排序必须是正数', IF_MUST, AC_BOTH],
    ];
    protected array $auto = [
        ['status', '1', 'string', IF_MUST, AC_INSERT],
    ];

    protected function _validate_where(array $data = []): array
    {
        return ['group_id' => $data['group_id']];
    }

    protected function _before_delete(array $data): void
    {
        $this->db = $this->db->where('is_sys=0');
    }
}