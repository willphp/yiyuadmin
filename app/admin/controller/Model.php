<?php
declare(strict_types=1);
namespace app\admin\controller;

class Model extends Base
{
    protected string $model = 'model';
    protected string $order = 'sort ASC,id DESC';

    // 获取模型标识(表名)
    protected function _get_name(int $model_id)
    {
        $name = widget('model')->get($model_id);
        if (!$name) {
            $this->error('模型不存在');
        }
        return $name;
    }

    // 生成数据表
    public function make_table(int $model_id)
    {
        $name = $this->_get_name($model_id);
        $fields = db('model_field')->where('model_id', $model_id)->column('field_name', 'id');
        if (!$fields) {
            $this->error('请先添加或导入模型字段，再生成！');
        }
        if (!in_array('id', $fields) || !in_array('status', $fields)) {
            $this->error('id字段和status字段必须存在，才能生成！');
        }
        $r = cli('make:table admin@'.$name.' _def'); // make:table 应用名@表名 来源模板 -f 覆盖生成
        $this->_jump(['数据表生成成功', '生成失败，请先删除原表再生成'], $r);
    }

    // 删除数据表
    public function remove_table(int $model_id)
    {
        $name = $this->_get_name($model_id);
        $r = cli('remove:table '.$name);
        $this->_jump(['数据表移除成功', '数据表移除失败'], $r);
    }

    // 生成MVC
    public function make_mvc(int $model_id)
    {
        $name = $this->_get_name($model_id);
        cli('make:ctrl admin@'.$name.' _def -f');
        cli('make:model admin@'.$name.' id _def -f');
        cli('make:view admin@'.$name.' index index -f');
        cli('make:view admin@'.$name.' add add -f');
        cli('make:view admin@'.$name.' edit edit -f');
        cli('clear:runtime admin');
        $this->success('MVC生成成功');
    }

    // 删除MVC
    public function remove_mvc(int $model_id)
    {
        $name = $this->_get_name($model_id);
        cli('remove:ctrl admin@'.$name);
        cli('remove:model admin@'.$name);
        cli('remove:view admin@'.$name);
        $this->success('MVC移除成功');
    }

    // 生成权限
    public function make_auth(int $model_id)
    {
        $model = db('model')->field('id,title,table_name')->where('id', $model_id)->where('status=1')->find();
        if (!$model) {
            $this->error('模型不存在');
        }
        $auth = model('auth');
        $isFind = $auth->where('model_id', '=', $model_id, 'or')->where('controller', $model['table_name'])->field('id')->find();
        if ($isFind) {
            $this->error('权限已存在');
        }
        $data = [];
        $data['title'] = $model['title'];
        $data['controller'] = $model['table_name'];
        $data['model_id'] = $model['id'];
        $data['sort'] = 100;
        $node_method = config_get('sys.default_node_method', 'index=列表', true);
        $data['method'] = array_keys($node_method);
        $r = $auth->save($data);
        $this->_jump(['生成成功，请到角色配置中添加操作权限', '权限生成失败'], $r);
    }

    // 删除权限
    public function remove_auth(int $model_id)
    {
        $name = $this->_get_name($model_id);
        db('auth')->where('controller', $name)->delete();
        db('auth_node')->where('controller', $name)->delete();
        $this->success('权限移除成功');
    }

    // 生成菜单
    public function make_menu(int $model_id)
    {
        $model = db('model')->field('id,title,table_name')->where('id', $model_id)->where('status=1')->find();
        if (!$model) {
            $this->error('模型不存在');
        }
        $menu = db('menu');
        $isFind = $menu->where('model_id', $model_id)->field('id')->find();
        if ($isFind) {
            $this->error('菜单已存在');
        }
        $data = [];
        $data['title'] = $model['title'];
        $data['pid'] = 19;
        $data['level'] = 2;
        $data['path'] = ',1,19,';
        $data['icon'] = 'layui-icon-face-smile-fine';
        $data['auth'] = $data['href'] = $model['table_name'] . '/index';
        $data['type'] = 1;
        $data['model_id'] = $model['id'];
        $data['sort'] = 100;
        $data['update_time'] = time();
        $data['status'] = 1;
        $menu->insert($data);
        widget('menu_tree')->reload();
        $this->success('生成成功，请到角色分组中配置权限');
    }

    // 删除菜单
    public function remove_menu(int $model_id)
    {
        $r = db('menu')->where('model_id', $model_id)->delete();
        widget('menu_tree')->reload();
        $this->_jump(['菜单移除成功', '菜单移除失败'], $r);
    }
}