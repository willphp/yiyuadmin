<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
class MakeModel extends Widget
{
    protected string $tag = 'model';
    protected int $expire = 0;
    public function set($id = '', array $options = []): array
    {
        $model_id = db('model')->where('table_name', $id)->value('id');
        if (!$model_id) {
            return [];
        }
        $model_field = widget('admin.model_field')->get($model_id); //模型字段
        $if = [1 => 'IF_MUST', 'IF_VALUE', 'IF_EMPTY', 'IF_ISSET', 'IF_UNSET']; // 条件
        $scene = [1 => 'AC_BOTH', 'AC_INSERT', 'AC_UPDATE']; // 场景
        $verify = $auto = $filter = '';
        foreach ($model_field as $vo) {
            if ($vo['is_verify']) {
                $verify .= "\n\t\t['{$vo['field_name']}', '{$vo['verify_rule']}', '{$vo['verify_msg']}', {$if[$vo['verify_if']]}, {$scene[$vo['verify_scene']]}],";
            }
            if ($vo['is_auto']) {
                $auto .= "\n\t\t['{$vo['field_name']}', '{$vo['auto_rule']}', '{$vo['auto_method']}', {$if[$vo['auto_if']]}, {$scene[$vo['auto_scene']]}],";
            }
            if ($vo['is_filter']) {
                $filter .= "\n\t\t['{$vo['field_name']}', {$if[$vo['filter_if']]}, {$scene[$vo['filter_scene']]}],";
            }
        }
        $data['verify'] = $verify ? "\n\tprotected array \$validate = [$verify\n\t];" : '';
        $data['auto'] = $auto ? "\n\tprotected array \$auto = [$auto\n\t];" : '';
        $data['filter'] = $filter ? "\n\tprotected array \$filter = [$filter\n\t];" : '';
        return $data;
    }
}