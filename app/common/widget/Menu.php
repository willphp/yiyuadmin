<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2024 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\common\widget;
use aphp\core\Widget;
/**
 * 导航主菜单
 */
class Menu extends Widget
{
    protected string $tag = 'cate';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        $menu = db('cate')->field('id,pid,title,sign,type,link,target,level,is_parent')->where('is_show=1 AND status=1')->order('sort ASC,id ASC')->select();
        return $this->_tree($menu, 1);
    }

    protected function _tree(array $menu, int $pid): array
    {
        $tree = [];
        foreach ($menu as $v) {
            $v['href'] = ($v['type'] == 3) ? url($v['link']) : url('@' . $v['sign']);
            unset($v['type'], $v['link']);
            if ($v['pid'] == $pid) {
                $v['children'] = $this->_tree($menu, $v['id']);
                $tree[] = $v;
            }
        }
        return $tree;
    }
}