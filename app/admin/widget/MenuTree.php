<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 获取菜单树
class MenuTree extends Widget
{
    protected string $tag = 'menu';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        $where = $options['where'] ?? [];
        $menu = db('menu')->field('id,pid,title,icon,href,target,type')->where($where)->where('pid>0 AND status=1')->order('sort ASC,id ASC')->select();
        return menu_tree($menu);
    }
}