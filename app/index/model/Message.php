<?php
declare(strict_types=1);
namespace app\index\model;
use aphp\core\Model;
/**
 * 留言模型，复制于app/admin/model/Message.php，修改模型后需同步修改
 */
class Message extends Model
{
	protected string $table = 'message';
	protected string $pk = 'id';

	protected array $validate = [
		['nickname', 'required', '昵称必填', IF_MUST, AC_BOTH],
		['email', 'email', '邮箱格式错误', IF_MUST, AC_BOTH],
		['msg', 'required', '留言内容必须', IF_MUST, AC_BOTH],
        ['captcha', 'captcha', '验证码不正确', IF_MUST, AC_BOTH], // 添加验证码
	];

	protected array $auto = [
		['ip', 'get_ip', 'function', IF_UNSET, AC_INSERT],
		['status', '1', 'string', IF_MUST, AC_INSERT],
        //['is_read', '1', 'string', IF_MUST, AC_INSERT], // 添加自动审核
	];

}