<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
class Field extends Widget
{
    protected string $tag = 'field';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        return db('field')->where('status=1')->order('sort ASC,id ASC')->column('label', 'id');
    }
}