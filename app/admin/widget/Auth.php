<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2024 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;

/**
 * 根据用户组ID获取用户权限
 */
class Auth extends Widget
{
    protected string $tag = 'admin_group';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        $auth = db('admin_group')->field('auth_menu,auth_node')->where('id', $id)->where('status=1')->find();
        return !empty($auth) ? $auth : ['auth_menu' => '', 'auth_node' => ''];
    }
}