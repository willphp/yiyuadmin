<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
// 权限节点
class AuthNode extends Model
{
    protected string $table = 'auth_node';
    protected string $pk = 'id';
    protected array $validate = [
        ['title', 'chs_dash', '节点名格式错误', IF_MUST, AC_BOTH],
        ['method', 'alpha_dash|unique', '方法名格式错误|方法名已存在', IF_MUST, AC_BOTH],
        ['sort', 'number', '排序必须是数字', IF_MUST, AC_BOTH],
    ];
    protected array $auto = [
        ['method', 'strtolower', 'function', IF_MUST, AC_BOTH],
        ['status', '1', 'string', IF_MUST, AC_INSERT],
    ];

    protected function _validate_where(array $data = []): array
    {
        return ['auth_id' => $data['auth_id']];
    }

    protected function _before_insert(array &$data): void
    {
        $data['controller'] =  db('auth')->where('id', $data['auth_id'])->value('controller'); // 添加时加入权限名称
    }
}