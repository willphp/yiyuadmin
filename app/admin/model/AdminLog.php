<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
class AdminLog extends Model
{
    protected string $table = 'admin_log';
    protected string $pk = 'id';
}