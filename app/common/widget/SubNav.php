<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2024 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\common\widget;
use aphp\core\Widget;
/**
 * 栏目二级导航
 */
class SubNav extends Widget
{
    protected string $tag = 'cate';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        $active_id = $options['active_id'] ?? 0;
        $list = db('cate')->field('id,title,sign,type,link,target')->where('id', '=', $id, 'or')->where('pid', $id)->where('is_show=1 AND status=1')->order('sort ASC,id ASC')->select();
        $data = [];
        $i = 1;
        foreach ($list as $vo) {
            $k = ($vo['id'] == $id) ? 0 : $i;
            if ($k == 0 && $vo['type'] == 2) {
                $vo['title'] = '全部';
            }
            $vo['href'] = ($vo['type'] == 3) ? url($vo['link']) : url('@'.$vo['sign']);
            $vo['is_active'] = $active_id == $vo['id'];
            $data[$k] = $vo;
            $i++;
        }
        ksort($data);
        $keys = array_column($data, 'id');
        return array_combine($keys, $data);
    }
}