<?php
declare(strict_types=1);
namespace app\admin\controller;
use aphp\core\Jump;
// 个人中心
class Profile
{
    use Jump;

    protected string $middleware = 'rbac';

    // 个人资料
    public function index()
    {
        $uid = session('user.id');
        if ($this->isAjax()) {
            $limit = input('get.limit', 20, 'intval');
            $page = input('get.page', 1, 'intval');
            $page = max(1,  $page);
            $log = db('admin_log');
            $list = $log->where('admin_id', $uid)->order('id DESC')->page($page, $limit)->select();
            $count = $log->where('admin_id', $uid)->count();
            $this->_json(200, '', $list, ['count' => $count]);
        }
        $user = model('admin')->find($uid);
        return view()->with(['vo' => $user->toArray()]);
    }

    // 修改
    public function edit(array $req)
    {
        $uid = session('user.id');
        $user = model('admin')->find($uid);
        if ($this->isPost()) {
            $req['id'] = $uid;
            $r = $user->save($req);
            $this->_jump(['修改成功', '修改失败'], $r, 'index');
        }
        $this->error('非法请求');
    }

    // 查看日志
    public function detail(int $id)
    {
        $uid = session('user.id');
        $vo = db('admin_log')->where('id', $id)->where('admin_id', $uid)->find();
        if (!$vo) {
            $this->error('记录不存在');
        }
        return view()->with('vo', $vo);
    }

    // 修改密码
    public function password(array $req)
    {
        if ($this->isPost()) {
            $model = model('admin');
            $r = $model->changePassword($req);
            $this->_jump(['修改成功', $model->getError()], $r, 'index');
        }
        return view();
    }

    // 头像设置
    public function avatar()
    {
        return view();
    }
}