<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 获取快捷菜单
class MenuWork extends Widget
{
    protected string $tag = 'menu';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        return db('menu')->where('id>2 AND type=1 AND status=1')->order('sort ASC,id ASC')->column('title', 'id');
    }
}