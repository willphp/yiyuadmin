<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 分类名
class Cate extends Widget
{
    protected string $tag = 'cate';
    protected int $expire = 0;

    public function set($id = '', array $options = [])
    {
        $map = [];
        if (isset($options['pid'])) {
            $map['pid'] = $options['pid'];
        }
        if (isset($options['cate_id'])) {
            $map['id'] = $options['cate_id'];
        }
        $list = db('cate')->where($map)->order('sort ASC,id ASC')->column('title', 'id');
        if (empty($id)) {
            return $list;
        }
        return $list[$id] ?? '';
    }
}