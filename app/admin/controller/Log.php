<?php
declare(strict_types=1);
namespace app\admin\controller;
// 操作日志
class Log extends Base
{
    protected string $model = 'admin_log';
    protected string $order = 'id DESC';
    protected string $fieldExcept = 'content,user_agent';

    public function clear(int $type = 0)
    {
        $map = [];
        if ($type == 1) {
            $month = strtotime('-1 month');
            $map[] = ['create_time', '<', $month];
            $msg = ['删除一月前日志成功', '一月前日志记录不存在'];
        } else {
            $map[] = ['id', '>', 0];
            $msg = ['清空日志成功', '清空日志失败'];
        }
        $r = db($this->model)->where($map)->delete();
        $this->_jump($msg, $r, 'index');
    }
}