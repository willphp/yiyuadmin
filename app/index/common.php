<?php
declare(strict_types=1);
/**
 * 前台函数库
 */
// 导航高亮显示
function nav_active(string $nav, string $class = ' class="active"', int $type = 1): string
{
    $active = ($type == 1) ? get_controller() : get_action();
    return ($nav == $active) ? $class : '';
}

// 获取随机缩略图
function rand_thumb(string $url, int $i): string
{
    if (!empty($url)) {
        return $url;
    }
    if ($i > 40) {
        $i = $i % 40;
    }
    return __STATIC__ . '/thumb/' . $i . '.jpg';
}

// 上一篇和下一篇
function get_related(int $id, string $table, array $options = []): array
{
    $data = widget('common.related')->get($table, $options);
    [$prev_id, $next_id] = get_prev_next($id, array_keys($data));
    $prev = $prev_id ? ['id' => $prev_id, 'title' => $data[$prev_id]] : [];
    $next = $next_id ? ['id' => $next_id, 'title' => $data[$next_id]] : [];
    return ['prev' => $prev, 'next' => $next];
}

// 获取头像
function get_avatar(string $url):string
{
    return !empty($url)? $url : __STATIC__.'/admin/images/avatar.png';
}