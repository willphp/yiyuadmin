<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2025 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace app\admin\controller;
class Cate extends Base
{
    protected string $model = 'cate'; // 模型表
    protected string $order = 'sort ASC,id ASC'; // 列表排序
    protected int $limit = 0; // 列表获取条数(0获取全部)
    protected string $fieldExcept = 'banner,summary,content,html_code,seo_title,seo_kw,seo_desc'; // 排除字段
    protected int $formSearch = 0; // 搜索开关

    protected function _list_parse(array $list): array
    {
        return $this->_tree($list);
    }

    protected function _tree(array $list, int $pid = 1): array
    {
        $tree = [];
        foreach ($list as $vo) {
            if ($vo['pid'] == $pid) {
                $vo['children'] = $this->_tree($list, $vo['id']);
                $tree[] = $vo;
            }
        }
        return $tree;
    }
}