## 一鱼CMS(AphpCMS)

10秒定制数据表一键生成CRUD。不同于主流框架的繁杂，致力于使用最少的代码。

### CMS特色

- 快速定制数据表一键生成CRUD
- 超简单模板标签调用数据
- 自由开发主题模板制作个人网站

### 在线演示

前台：http://demo.aphpcms.com 后台：http://demo.aphpcms.com/admin.php

### 技术构架

- PHP环境：PHP7.4 ~ PHP8.4
- 数据库：MySQL5.6 ~ MySQL8.0
- PHP框架：APHP5.3.x
- 后台UI：PearAdmin3.x(Layui2.9.23)

### 安装指南

1. 下载安装包后上传到网站目录并解压
2. 设置运行目录到 `/public`
3. 设置伪静态(查看url_rewrite.txt文件)
4. 修改数据库配置 `config/database.php`
5. 浏览器访问 `/install.php` 安装

### 错误处理

提示：页面出错！请稍后再试～

1. 在 `config/app.php` 开启 `debug=>true` 显示错误详细。 
2. 查看 `runtime/应用名/log` 下最新日志信息。
3. 清空 `runtime` 目录，或访问 `api/clear` 清空缓存。

### 相关链接

文档：https://doc.aphp.top  CMS：https://aphpcms.com 框架：https://aphp.top

### 技术支持

QQ群1：325825297 QQ群2：16008861 作者：无念(24203741@qq.com) 