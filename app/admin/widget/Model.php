<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 获取模型表名
class Model extends Widget
{
    protected string $tag = 'model';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        if (!empty($id)) {
            return db('model')->where('id', $id)->where('status=1')->value('table_name');
        }
        return db('model')->where('status=1')->order('id ASC')->column('table_name', 'id');
    }
}