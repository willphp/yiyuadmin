<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
class Dict extends Model
{
	protected string $table = 'dict';
	protected string $pk = 'id';
    protected array $validate = [
        ['sign', 'string|unique', '标识格式错误|标识已存在', IF_MUST, AC_BOTH],
        ['title', 'required', '名称必须', IF_MUST, AC_BOTH],
        ['data', 'required', '数据必须', IF_MUST, AC_BOTH],
    ];
	protected array $auto = [
        ['status', 1, 'string', IF_MUST, AC_INSERT],
    ];
}