<?php
declare(strict_types=1);
namespace app\admin\controller;
// 配置分组
class ConfigGroup extends Base
{
    protected string $model = 'config_group';
    protected string $order = 'sort ASC,id ASC';

    public function remove_config(int $id)
    {
        db('config')->where('group_id', $id)->delete();
        cli('clear:runtime admin');
        $this->success('清空配置项成功', 'index');
    }
}