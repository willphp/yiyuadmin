<?php
/*------------------------------------------------------------------
 | Software: APHP - A PHP TOP Framework
 | Site: https://aphp.top
 |------------------------------------------------------------------
 | (C)2020-2025 无念<24203741@qq.com>,All Rights Reserved.
 |-----------------------------------------------------------------*/
declare(strict_types=1);
namespace middleware;
use Closure;
/**
 * 全局中间件
 */
class Boot
{
    public function run(Closure $next): void
    {
        header('X-Powered-By:APHP' . __VERSION__);
        $install_check = config_get('app.install_check', []); // 获取需要检测安装的应用列表
        if (!empty($install_check)) {
            // 检测安装
            if (in_array(APP_NAME, $install_check) && !file_exists(ROOT_PATH . '/backup/install.lock') && !str_contains($_SERVER['REQUEST_URI'], 'install')) {
                header('Location:'.__HOST__.'/install.php');
                exit();
            }
        }
        // 检测开启状态
        if (APP_NAME === 'index' && site('is_open', 1) == 0) {
            echo site('close_msg');
            exit();
        }
        $next();
    }
}