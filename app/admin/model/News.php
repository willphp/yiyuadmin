<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
// 生成时间：2025-03-02 15:40:41
class News extends Model
{
	protected string $table = 'news';
	protected string $pk = 'id';

	protected array $validate = [
		['title', 'required', '标题必填', IF_MUST, AC_BOTH],
		['url', 'url', '链接地址错误', IF_VALUE, AC_BOTH],
		['read_count', 'number', '积分必须是数字', IF_MUST, AC_BOTH],
		['sort', 'number', '排序必须是正数', IF_ISSET, AC_BOTH],
	];

	protected array $auto = [
		['post_time', 'strtotime', 'function', IF_MUST, AC_BOTH],
		['status', '1', 'string', IF_MUST, AC_INSERT],
	];

}