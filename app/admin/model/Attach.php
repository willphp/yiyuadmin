<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
use aphp\core\Tool;

class Attach extends Model
{
    protected string $table = 'attach';
    protected string $pk = 'id';
    protected array $auto = [
        ['status', '1', 'string', IF_MUST, AC_INSERT],
    ];

    public function getSizeAttr(int $val, array $data): string
    {
        return Tool::size2kb($val);
    }

    protected function _after_delete(array $data): void
    {
        $file = ROOT_PATH . '/public' . trim($data['path'], '.');
        if (file_exists($file)) {
            unlink($file);
        }
    }
}