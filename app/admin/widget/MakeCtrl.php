<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
class MakeCtrl extends Widget
{
    protected string $tag = 'model';
    protected int $expire = 0;
    public function set($id = '', array $options = []): array
    {
        return db('model')->field('table_name,list_order,list_limit,list_except,form_search,url_search,is_recycle')->where('table_name', $id)->find();
    }
}