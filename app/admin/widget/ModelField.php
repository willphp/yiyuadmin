<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 根据模型ID获取模型字段
class ModelField extends Widget
{
    protected string $tag = 'model_field';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        $data = db('model_field')->where('model_id', $id)->where('status=1')->order('sort ASC,id ASC')->select();
        $data = array_column($data, null, 'field_name'); //去除重复
        return array_values($data); // 去除键名，重新排序
    }
}