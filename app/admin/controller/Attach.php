<?php
declare(strict_types=1);
namespace app\admin\controller;
// 附件管理
class Attach extends Base {
	protected string $model = 'attach';
	protected string $order = 'sort ASC,id DESC';
    protected int $urlSearch = 1;

    // 附件选择
    public function selected()
    {
        return view();
    }
}