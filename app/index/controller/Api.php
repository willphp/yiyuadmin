<?php
declare(strict_types=1);
namespace app\index\controller;
use aphp\core\Jump;
use extend\captcha\Captcha;

class Api
{
	use Jump;
    public function clear()
    {
        cache_clear();
        cli('clear:runtime index common');
        $this->success('清除缓存成功', 'index/index');
    }

    // 添加留言
    public function message_add(array $req)
    {
        if ($this->isPost()) {
            $r = model('message')->save($req);
            $this->_jump(['留言成功，请耐心等待审核和回复...', '留言失败，请稍后重试'], $r, 'message/index');
        }
        $this->error('非法操作');
    }

    // 验证码
    public function captcha()
    {
        return (new Captcha())->make();
    }
}