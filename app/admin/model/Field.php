<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
class Field extends Model
{
	protected string $table = 'field';
	protected string $pk = 'id';
    protected array $validate = [
        ['label', 'required', '标签名必须', IF_MUST, AC_BOTH],
        ['field_title', 'required', '字段标题必须', IF_MUST, AC_BOTH],
        ['field_name', '/^\w{2,20}$/', '字段名称格式错误', IF_MUST, AC_BOTH],
        ['sort', 'number', '排序值必须是数字', IF_ISSET, AC_BOTH],
    ];
    protected array $auto = [
        ['field_name', 'strtolower', 'function', IF_MUST, AC_BOTH],
        ['field_comment', 'field_title', 'field', IF_EMPTY, AC_BOTH],
        ['status', '1', 'string', IF_MUST, AC_INSERT],
    ];

    protected function _before_insert(array &$data): void
    {
        $data['is_primary'] = ($data['type_id'] == 2) ? 1 : 0; // 主键
        $data['is_auto_increment'] = $data['is_primary']; // 自增
        //$data['form_title'] = $data['field_title']; // 表单标题
    }
}