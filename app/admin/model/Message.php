<?php
declare(strict_types=1);
namespace app\admin\model;
use aphp\core\Model;
// 生成时间：2025-03-02 13:18:12
class Message extends Model
{
	protected string $table = 'message';
	protected string $pk = 'id';

	protected array $validate = [
		['nickname', 'required', '昵称必填', IF_MUST, AC_BOTH],
		['email', 'email', '邮箱格式错误', IF_MUST, AC_BOTH],
		['msg', 'required', '留言内容必须', IF_MUST, AC_BOTH],
		['sort', 'number', '排序必须是正数', IF_ISSET, AC_BOTH],
	];

	protected array $auto = [
		['ip', 'get_ip', 'function', IF_UNSET, AC_INSERT],
		['status', '1', 'string', IF_MUST, AC_INSERT],
	];

}