<?php
declare(strict_types=1);
namespace app\admin\controller;
// 权限节点管理
class Auth extends Base
{
    protected string $model = 'auth';
    protected string $order = 'sort ASC,id ASC';
    protected  int $formSearch = 0;

    // 节点列表
    public function node(int $id)
    {
        if ($this->isAjax()) {
            $where = ['auth_id' => $id];
            $this->model = 'auth_node';
            $this->_list_json($where);
        }
        $this->error('非法请求');
    }

    // 添加节点
    public function node_add(int $auth_id, array $req)
    {
        $this->model = 'auth_node';
        view_with('auth_id', $auth_id);
        return $this->add($req);
    }

    // 编辑节点
    public function node_edit(int $id, array $req)
    {
        $this->model = 'auth_node';
        return $this->edit($id, $req);
    }

    // 删除节点
    public function node_del(string $ids)
    {
        $this->model = 'auth_node';
        $this->destroy($ids);
    }

    // 节点更新字段
    public function node_multi(array $req)
    {
        $this->model = 'auth_node';
        $this->multi($req);
    }
}