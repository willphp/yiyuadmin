<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
class ModelSelect extends Widget
{
    protected string $tag = 'model';
    protected int $expire = 0;
    public function set($id = '', array $options = [])
    {
        $list = db('model')->where('status=1')->order('id ASC')->column('title', 'id');
        if (!empty($id)) {
            return $list[$id] ?? '=未设置=';
        }
        return ['无'] + $list;
    }
}