<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
use extend\layui\Form;
// 配置表单
class ConfigForm extends Widget
{
    protected string $tag = 'config_group';
    protected int $expire = 0;

    public function set($id = '', array $options = [])
    {
        if (!empty($id)) {
            $vo = db('config_group')->field('id,title,memo,app,sign')->where('id', $id)->where('status=1')->find();
            if ($vo) {
                $vo['html'] = $this->_get_form($vo['id'], $vo['sign']);
            }
            return $vo;
        }
        $list = db('config_group')->field('id,title,memo,app,sign')->where('is_hide=0 AND status=1')->order('sort ASC,id ASC')->select();
        foreach ($list as &$vo) {
            $vo['html'] = $this->_get_form($vo['id'], $vo['sign']);
        }
        return $list;
    }

    private function _get_form(int $group_id, string $file): string
    {
        $list = db('config')->where('group_id', $group_id)->where('status=1')->order('sort ASC,id ASC')->select();
        $form = Form::init($group_id);
        $html = [];
        foreach ($list as $vo) {
            $append = '<a href="'.url('config/edit', ['id'=>$vo['id']]).'" title="编辑"><i class="layui-icon layui-icon-edit"></i></a>';
            $append .= ' <a href="javascript:;" data-event="del" data-id="'.$vo['id'].'"><i class="layui-icon layui-icon-delete"></i></a>';
            $title = '复制标签';
            $tag = ($file == 'site') ? '{:site(\''.$vo['field_name'].'\')}' : '{:config(\''.$file.'.'.$vo['field_name'].'\')}';
            $append .= ' <span class="layui-font-purple copy" title="'.$title.'">'.$tag.'</span>';
            $html[] = $form->make($vo, 2, $append);
        }
        return implode('', $html);
    }
}