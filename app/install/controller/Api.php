<?php
declare(strict_types=1);
namespace app\install\controller;
use aphp\core\Jump;
class Api
{
	use Jump;
    public function clear()
    {
        cache_clear();
        cli('clear:runtime install');
        $this->success('清除缓存成功', 'index/index');
    }
}