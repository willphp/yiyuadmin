<?php
declare(strict_types=1);
namespace app\index\controller;
use aphp\core\Jump;

class Index
{
    use Jump;

    // 首页
    public function index(string $act = 'index/index')
    {
        $site = site();
        $cats = [
            'seo_title' => $site['site_title'],
            'seo_kw' => $site['site_kw'],
            'seo_desc' => $site['site_desc'],
            'tpl' => $act,
        ];
        view_with('site', $site);
        // 首页
        if ($act == 'index/index') {
            return view('index/index')->with('cats', $cats);
        }
        [$class, $method] = explode('/', $act);
        $cats = widget('common.cate')->get($class);
        if (!empty($cats)) {
            $tpl = ($method == 'index') ? $cats['tpl'] : $cats['tpl'] . '_' . $method;
            if (is_file(VIEW_PATH . '/' . __THEME__ . '/index/' . $tpl . '.html')) {
                $cats['tpl'] = 'index/' . $tpl;
            } else  {
                $cats['tpl'] = ($cats['type'] == 1) ? 'index/about' : 'index/404';
            }
        }
        if (empty($cats) || $cats['tpl'] == 'index/404') {
            if ($this->isAjax()) {
                $this->_json(404, '页面不存在');
            }
            return view('index/404')->with('cats', $cats);
        }
        $table = $cats['model_table'];
        if (!empty($table) && $cats['type'] == 2) {
            // 列表详情
            if ($method == 'detail') {
                $id = input('get.id', 0, 'intval');
                if ($id <= 0) {
                    $this->error('ID参数必须');
                }
                $vo = db($table)->where('id', $id)->where('status=1')->find();
                if (!$vo) {
                    $this->error('记录不存在');
                }
                $cats['seo_title'] = $vo['title'] . ' - ' . $site['site_name'];
                if (!empty($vo['seo_kw'])) {
                    $cats['seo_kw'] = $vo['seo_kw'];
                }
                if (!empty($vo['seo_desc'])) {
                    $cats['seo_desc'] = $vo['seo_desc'];
                }
                if (isset($vo['read_count']) && $id != cookie($table . '-read-id')) {
                    db($table)->where('id', $id)->setInc('read_count');
                    cookie($table . '-read-id', $id);
                    $vo['read_count']++;
                }
                return view($cats['tpl'])->with(['cats' => $cats, 'vo' => $vo]);
            }
            // 列表
            if ($method == 'index') {
                // 列表条件
                $page_where = [];
                $page_type = 1;
                if ($cats['id'] != $cats['nav_pid'] && $cats['is_parent'] == 0) {
                    $page_where['tid'] = $cats['id'];
                    $page_type = 2;
                }
                $keyword = input('get.keyword', '', 'clear_html');
                if (!empty($keyword)) {
                    $page_where[] = ['title', 'like', "%{$keyword}%"];
                    $page_type = 3;
                }
                $tag = input('get.tag', '', 'clear_html');
                if (!empty($tag)) {
                    $tags = widget('common.tags')->get('', ['field' => 'id,title']);
                    $tag_id = $tags[$tag]['id'] ?? 0;
                    if ($tag_id > 0) {
                        $page_where[] = ['tags_ids', 'find_in_set', $tag_id];
                    }
                    $tag = $tags[$tag]['title'] ?? $tag;
                    $page_type = 4;
                }
                return view($cats['tpl'])->with(['cats' => $cats, 'page_where' => $page_where, 'page_type' => $page_type, 'keyword' => $keyword, 'tag' => $tag]);
            }
        }
        return view($cats['tpl'])->with('cats', $cats);
    }
}