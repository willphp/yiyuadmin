<?php
declare(strict_types=1);
namespace app\admin\controller;
class Field extends Base
{
    protected string $model = 'field';
    protected string $order = 'sort ASC,id ASC';
}