<?php
declare(strict_types=1);
namespace app\admin\widget;
use aphp\core\Widget;
// 菜单目录
class MenuDir extends Widget
{
    protected string $tag = 'menu';
    protected int $expire = 0;

    public function set($id = '', array $options = []): array
    {
        $menu = db('menu')->field('id,pid,title,level')->where('type=0 AND status=1')->order('sort ASC,id ASC')->select();
        $menu =  $this->_sort($menu);
        return array_column($menu, 'title', 'id');
    }

    protected function _sort(array $menu, int $pid = 0): array
    {
        $data = [];
        foreach($menu as $v){
            if($v['pid'] == $pid){
                if ($v['level'] > 0) {
                    $v['title'] = str_repeat('&nbsp;&nbsp;&nbsp;', $v['level']).'├─'.$v['title'];
                }
                $data[] = $v;
                $data = array_merge($data, $this->_sort($menu, $v['id']));
            }
        }
        return $data;
    }
}